# To do

## Bugfixes and refactoring

## Minor
- Should `getKeys` be part of interface?

## Major
- Naming: DependencyManager?
- Are definitions other than `Closure` really needed?
