# Changelog

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# [Unreleased]
### Changes
- Optimized Gitlab CI

# [3.0.0] — 2022-06-13
### Changes
- Stronger typing in `ServiceContainer` and consequently `BasicServiceContainer`
- `null` (instead of `false`) when service not found.
### Added
- Static analysis with Psalm
- Testing with PHP 8.1

## [2.1.3] — 2020-12-11
### Added
- Rough documentation

## [2.1.2] — 2020-12-11
### Changed
- Configuration improvements

## [2.1.1] — 2020-12-09
### Added
- Allowing PHP 8.0

## [2.1.0] — 2019-12-06
### Added
- Method `getKeys` returning all defined keys
### Changed
- Less noise in Gitlab CI
### Fixed
- Unit test configuration

## [2.0.3] — 2019-11-29
## Changed
- Updated missing changelog entries

## [2.0.2] — 2019-11-29
### Fixed
- Fixed PHP Unit configuration

## [2.0.1] — 2019-11-29
### Changed
- Reorganized composer configuration
- Reorganized PHP Unit configuration

## [2.0.0] — 2019-11-29
### Changed
- PHP requirement upgrade to 7.4
- Interface name to `ServiceContainer`
- `BasicServiceContainer` is final now

## [1.0.4] — 2019-01-02
### Added
- MIT licence

## [1.0.3] — 2019-01-02
### Added
- CodeSniffer as a development dependency
- Coding standard pipeline

## [1.0.2] — 2019-01-02
### Added
- Gitlab-CI support for development
- This changelog

## [1.0.1] — 2019-01-02
### Added
- PHPUnit as a development dependency

## [1.0.0] — 2018-05-31
### Added
- Simple dependency container
