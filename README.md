# Service Container

This repository provides a very simple implementation of service container in PHP.
It allows separating the object instantiation logic from the usage of the object itself.
Also, by means of lazy loading, it makes it possible to instantiate only those objects that are actually used
and by centralizing the instantiation process it makes sure no redundant instances of the same class is created.
Finally, it allows easy refactoring: one definition can be replaced with another one by change in one place. 

## Content

* `ServiceContainer` interface
* `BasicServiceContainer` implementation of `ServiceContainer`

## Usage

### Initialization

`BasicServiceContainer` is initialized with an array defining services.

```php
use Zalmoksis\ServiceContainer\{BasicServiceContainer, ServiceContainer};
use MyNamespace\MyClass;

$serviceContainer = new BasicServiceContainer([
    'service1' => function (ServiceContainer $serviceContainer) {
        return new MyClass();
    },
    'service2' => MyClass::class,
    'service3' => new MyClass(),
]);
```
Three kinds of definitions are possible:
* `Closure` -- the recommended one. It is executed upon the first request for the service.
  It also has access to the service container itself, allowing injection of dependencies to the object being created.
* fully qualified class name -- The object is instantiated upon the first request for the service.
  It is of very limited use as no parameters can be passed.
* object instance -- The returned object will be the same instance.

### Obtaining services

Defined services can be obtained using `BasicServiceContainer::get()` method.

```php
$serviceContainer->get('service1')->doSomething();
```

If a service was defined by `Closure` or class name, it is not instantiated until the first call of `get()`
and all further calls will return the same instance.

It is also possible to check if a service was defined without trying to instantiate it,
using `BasicServiceContainer::has()` method.

```php
if ($serviceContainer->has('service2')) {
    $serviceContainer->get('service2');
}
```

### Recommended usage

It is recommended not to pass the service container itself to the instantiated objects.
It should rather serve the purpose of encapsulating the creation logic according to the inversion of control principle.

```php
use Zalmoksis\ServiceContainer\{ServiceContainer, BasicServiceContainer};
use MyNamespace\{MyClass1, MyClass2, MyClass3, MyClass4};

$serviceContainer = new BasicServiceContainer([
    'service1' => function (ServiceContainer $serviceContainer) {
        return new MyClass1(
            $serviceContainer->get('service2'),
            $serviceContainer->get('service4'),
        );
    },
    'service2' => function (ServiceContainer $serviceContainer) {
        return new MyClass2($serviceContainer->get('service3'));
    },
    'service3' => function (ServiceContainer $serviceContainer) {
        return new MyClass3();
    },
    'service4' => function (ServiceContainer $serviceContainer) {
        return new MyClass4();
    },
]);
```

### Defining on the fly

It is also possible to define or redefine services in already instantiated `BasicServiceContainer`.
This can be achieved using `BasicServiceContainer::set()`.

```php
$serviceContainer
    ->set('service1', function(ServiceContainer $serviceContainer){ return new MyClass1(); })
    ->set('service2', MyClass2::class)
    ->set('service3', new MyClass3())
;
```

However a word of warning is required:
if a service has already been instantiated, the change of definition won't affect it.

```php
$serviceContainer = new BasicServiceContainer(['service' => MyClass1::class]);

$service = $serviceContainer->get('service'); // MyClass1

$serviceContainer->set('service', MyClass2::class);

$service = $serviceContainer->get('service'); // still MyClass1
```
