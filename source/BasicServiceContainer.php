<?php

namespace Zalmoksis\ServiceContainer;

use Closure;

final class BasicServiceContainer implements ServiceContainer {
    private array $definitions  = [];
    private array $instances    = [];

    //-----------------------------------------------------
    // allowed definitions:
    //  - callback(ServiceContainerInterface $serviceContainer) returning an instance
    //  - string - name of the class to be instantiated
    //  - object instance
    //-----------------------------------------------------
    function __construct(array $definitions = []) {
        $this->definitions = $definitions;
    }

    /**
     * @param Closure|object|string $definition
     */
    function set(string $key, $definition): BasicServiceContainer {
        $this->definitions[$key] = $definition;

        return $this;
    }

    function has(string $key): bool {
        return isset($this->definitions[$key]);
    }

    function get(string $key): ?object {
        if (!array_key_exists($key, $this->instances)) {
            $this->construct($key);
        }

        return $this->instances[$key];
    }

    function getKeys(): array {
        return array_keys($this->definitions);
    }

    private function construct(string $key): void {
        $this->instances[$key] = array_key_exists($key, $this->definitions)
            ? $this->resolveDefinition($this->definitions[$key])
            : null;
    }

    /**
     * @param Closure|object|string $definition
     */
    private function resolveDefinition($definition): ?object {
        if ($definition instanceof Closure) {
            return $definition($this);
        }

        if (is_string($definition)) {
            return new $definition();
        }

        if (is_object($definition)) {
            return $definition;
        }

        return null;
    }
}
