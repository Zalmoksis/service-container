<?php

namespace Zalmoksis\ServiceContainer;

interface ServiceContainer {
    /**
     * @param Closure|object|string $definition
     */
    function set(string $key, $definition): ServiceContainer;
    function has(string $key): bool;
    function get(string $key): ?object;
}
