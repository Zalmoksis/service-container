<?php

namespace Zalmoksis\ServiceContainer\Tests;

use PHPUnit\Framework\TestCase;
use stdClass;
use Zalmoksis\ServiceContainer\BasicServiceContainer;
use Zalmoksis\ServiceContainer\ServiceContainer;

final class BasicServiceContainerTest extends TestCase {
    private BasicServiceContainer $basicServiceContainer;

    function setUp(): void {
        $this->basicServiceContainer = new BasicServiceContainer([
            'definition-by-closure'  => fn(ServiceContainer $serviceContainer) => new stdClass(),
            'definition-by-instance' => new stdClass(),
            'definition-by-string'   => 'stdClass',
            'invalid-definition'     => [],
        ]);
        $this->basicServiceContainer->set('runtime-definition', 'stdClass');
    }

    function testIfBasicServiceContainerImplementsServiceContainerInterface() {
        $this->assertInstanceOf(
            ServiceContainer::class,
            $this->basicServiceContainer
        );
    }

    function testCheckingIfServiceContainerHaveKeys() {
        $this->assertTrue($this->basicServiceContainer->has('definition-by-closure'));
        $this->assertTrue($this->basicServiceContainer->has('definition-by-instance'));
        $this->assertTrue($this->basicServiceContainer->has('definition-by-string'));
        $this->assertTrue($this->basicServiceContainer->has('invalid-definition'));
        $this->assertTrue($this->basicServiceContainer->has('runtime-definition'));
        $this->assertFalse($this->basicServiceContainer->has('definition-not-provided'));
    }

    function testIfBasicServiceContainerReturnsInstances() {

        $this->assertInstanceOf(
            'stdClass',
            $serviceDefinedByClosure = $this->basicServiceContainer->get('definition-by-closure')
        );

        $this->assertInstanceOf(
            'stdClass',
            $serviceDefinedByInstance = $this->basicServiceContainer->get('definition-by-instance')
        );

        $this->assertInstanceOf(
            'stdClass',
            $serviceDefinedByString = $this->basicServiceContainer->get('definition-by-string')
        );

        $this->assertInstanceOf(
            'stdClass',
            $serviceDefinedInRuntime = $this->basicServiceContainer->get('runtime-definition')
        );

        // asserting the same instance is returned with the second call

        $this->assertSame(
            $serviceDefinedByClosure,
            $this->basicServiceContainer->get('definition-by-closure')
        );

        $this->assertSame(
            $serviceDefinedByInstance,
            $this->basicServiceContainer->get('definition-by-instance')
        );

        $this->assertSame(
            $serviceDefinedByString,
            $this->basicServiceContainer->get('definition-by-string')
        );

        $this->assertSame(
            $serviceDefinedInRuntime,
            $this->basicServiceContainer->get('runtime-definition')
        );
    }

    function testGettingServiceThatWasNotDefined() {
        $this->assertNull($this->basicServiceContainer->get('definition-not-provided'));
    }

    function testGettingServiceWithInvalidDefinition() {
        $this->assertNull($this->basicServiceContainer->get('invalid-definition'));
    }

    function testGettingKeys() {
        $this->assertEquals([
            'definition-by-closure',
            'definition-by-instance',
            'definition-by-string',
            'invalid-definition',
            'runtime-definition',
        ], $this->basicServiceContainer->getKeys());
    }
}
